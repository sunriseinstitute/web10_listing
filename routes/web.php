<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ------------- Website Routes -----------------//
Route::get('/', [App\Http\Controllers\Front\HomeController::class, 'home'])->name('web.home');
Route::get('/detail/{id}', [App\Http\Controllers\Front\HomeController::class, 'detail'])->name('web.detail');
Route::get('/product-promotion', [App\Http\Controllers\Front\HomeController::class, 'promotion'])->name('web.promotion');
Route::get('/product/{category_id}', [App\Http\Controllers\Front\HomeController::class, 'productCategory'])->name('web.category');
// -------------End Website Routes -----------------//


// ------------ Admin Routes ------------------//

Auth::routes(['register' => false]);

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
// Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// bulk route 
Route::get('/bulk', [App\Http\Controllers\BulkController::class, 'index'])->name('bulk.index');
Route::post('/bulk/save', [App\Http\Controllers\BulkController::class, 'save'])->name('bulk.save');
Route::post('/bulk/update', [App\Http\Controllers\BulkController::class, 'update'])->name('bulk.update');
Route::post('/bulk/delete', [App\Http\Controllers\BulkController::class, 'delete'])->name('bulk.delete');
Route::post('/bulk/getone', [App\Http\Controllers\BulkController::class, 'getOne'])->name('bulk.getone');

// User 
Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('user.index');
Route::post('/user/save', [App\Http\Controllers\UserController::class, 'save'])->name('user.save');
Route::post('/user/update', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
Route::post('/user/delete', [App\Http\Controllers\UserController::class, 'delete'])->name('user.delete');

// Role 
Route::get('/role', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
Route::post('/role/save', [App\Http\Controllers\RoleController::class, 'save'])->name('role.save');
Route::post('/role/update', [App\Http\Controllers\RoleController::class, 'update'])->name('role.update');
Route::post('/role/delete', [App\Http\Controllers\RoleController::class, 'delete'])->name('role.delete');

// permission 
Route::get('/permission', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
Route::post('/permission/save', [App\Http\Controllers\PermissionController::class, 'save'])->name('permission.save');

// Set Permission 
Route::get('/set-permission/{role_id}', [App\Http\Controllers\RolePermissionController::class, 'index'])->name('role_permission.index');
Route::post('/set-permission/update', [App\Http\Controllers\RolePermissionController::class, 'updatePermission'])->name('role_permission.updatePermission');

// Category 
Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');

// slideshow 
Route::get('/slideshow', [App\Http\Controllers\SlideshowController::class, 'index'])->name('slideshow.index');
Route::get('no-permission', function(){
    return view('permissions.no_permission');
})->name('no_permission');

// Product 
Route::get('/product', [App\Http\Controllers\ProductController::class, 'index'])->name('product.index');
// Promotion
Route::get('/promotion', [App\Http\Controllers\ProductPromotionController::class, 'index'])->name('promotion.index');


Route::get('/test', function(){
    // checkPermissiion('category', 'view');
    // echo request()->header('User-Agent')
    $agent = new \Jenssegers\Agent\Agent;
    echo ($agent->robot()).'<br>';
    echo ($agent->platform()).'<br>';
    echo ($agent->version($agent->platform())).'<br>';
    echo ($agent->browser()).'<br>';
    echo ($agent->version($agent->browser())).'<br>';
    echo $agent->device().'<br>';
    echo request()->ip().'<br>';
});

//  // role 
// Route::get('/role', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
// Route::get('/role/save', [App\Http\Controllers\RoleController::class, 'save'])->name('role.save');
// Route::get('/role/delete/{id}', [App\Http\Controllers\RoleController::class, 'delete'])->name('role.delete');

// Route::get('/role/permission/{id}', [App\Http\Controllers\RolePermissionController::class, 'index'])->name('role.permission');
// Route::get('/rolepermission/generate', [App\Http\Controllers\RolePermissionController::class, 'generateRolePermission'])->name('generate_role_permission');
// Route::get('/rolepermission/save', [App\Http\Controllers\RolePermissionController::class, 'save']);

// //  permission
// Route::get('/permission', [App\Http\Controllers\PermissionController::class, 'index'])->name('permission.index');
// Route::get('/permission/save', [App\Http\Controllers\PermissionController::class, 'store'])->name('permission.save');
// Route::get('/permission/update', [App\Http\Controllers\PermissionController::class, 'update'])->name('permission.update');
