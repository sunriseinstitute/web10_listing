<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Models\Permission;
use App\Models\RolePermission;
use App\Models\Role;

class PermissionController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = Permission::where('active', 1);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('permissions.index');
    }

    public function save(Request $r){
        $permission = new Permission();
        $permission->name = $r->name;
        $permission->alias = $r->alias;
        if($permission->save()){
            $roles = Role::select('id')->where('active', 1)->get();
            foreach($roles as $role){
                $role_per = new RolePermission();
                $role_per->role_id = $role->id;
                $role_per->permission_id = $permission->id;
                $role_per->view = 0;
                $role_per->create = 0;
                $role_per->update = 0;
                $role_per->delete = 0;
                $role_per->save();
            }
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data saved successfully"
                ]
            );
        }else {
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data not saved!"
                ]
            );
        }
    }

}
