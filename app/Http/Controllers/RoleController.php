<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\Permission;

class RoleController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = Role::where('active', 1);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('rolename', function($row){
                    $role_name = "<a href='".route('role_permission.index', $row->id)."'>$row->name</a>";
                    return $role_name;
                })
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action', 'rolename'])
                ->make(true);
        }
        return view('roles.index');
    }

    function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $role = new Role();
        $role->name = $request->name;
        if($role->save()){
            $permissions = Permission::select('id')->where('active', 1)->get();
            foreach($permissions as $per){
                $role_per = new RolePermission();
                $role_per->role_id = $role->id;
                $role_per->permission_id = $per->id;
                $role_per->view = 0;
                $role_per->create = 0;
                $role_per->update = 0;
                $role_per->delete = 0;
                $role_per->save();
            }
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data saved successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data was not saved1!"
                ]
            );
        }
    }

    function update(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages());
        }

        $role = Role::find($request->id);
        $role->name = $request->name;
        
        if($role->save()){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data updated successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data was not updated!"
                ]
            );
        }
    }

    function delete(Request $request){
        // DB::table('users')
        //     ->where('id', $request->id)
        //     ->update(['active' => 0]);
        $role = Role::find($request->id);
        $role->active = 0;
        if($role->save()){
            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Deleted successfully',
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => 'Unable to delete this recored!',
                ]
            );
        }
    }
}
