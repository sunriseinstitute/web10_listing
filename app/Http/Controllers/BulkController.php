<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BulkController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = DB::table($request->table_name);
            if($request->active){
                $data =  $data->where('active', 1);
            }
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view($request->view_path);
    }

    public function save(Request $request){
        $data = $request->except('_token', 'table_name', 'photo');
        if($request->hasFile('photo')){
            $photo = $request->file('photo'); // get image object
            $photo_name = strtotime("now"). '.' . $photo->getClientOriginalExtension(); // rename photo name and keep origin extesion
            $upload_destination = public_path('assets/img'); // where you store image in public folder.
            
            if($photo->move($upload_destination, $photo_name)){
                $data['photo'] = $photo_name;
            }
        }
        $i = DB::table($request->table_name)->insert($data);
        if($i){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data saved successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data not saved!"
                ]
            );
        }
    }

    public function update(Request $request){
        $data = $request->except('_token', 'table_name', 'id', 'photo');
        if($request->hasFile('photo')){
            $photo = $request->file('photo'); // get image object
            $photo_name = strtotime("now"). '.' . $photo->getClientOriginalExtension(); // rename photo name and keep origin extesion
            $upload_destination = public_path('assets/img'); // where you store image in public folder.
            
            if($photo->move($upload_destination, $photo_name)){
                $data['photo'] = $photo_name;
            }
        }
        $i = DB::table($request->table_name)
            ->where('id', $request->id)
            ->update($data);
        if($i){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data updated successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data not updated!"
                ]
            );
        }
    }

    public function delete(Request $request){
        $i = DB::table($request->table_name)
            ->where('id', $request->id)
            ->update(['active' => 0]);
        if($i){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data updated successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data not updated!"
                ]
            );
        }
    }

    public function getOne(Request $request){
        $data = DB::table($request->table_name)->find($request->id);
        return response()->json(
            [
                'status' => 200,
                'data' => $data,
                'message' => "Data updated successfully"
            ]
        );
    }
}
