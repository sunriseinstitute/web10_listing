<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Models\User;
use DB;

class UserController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = User::join('roles', 'roles.id', 'users.role_id')
                ->select(
                    'users.*',
                    'roles.name as role_name'
                )
                ->where('users.active', 1);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $data['roles'] = DB::table('roles')->where('active', 1)->get();
        return view('users.index', $data);
    }

    protected function validator($data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()) {    
            return response()->json($validator->messages());
        }
    }

    function save(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'role' => ['required'],
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages());
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role;
        if($user->save()){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data saved successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data was not saved1!"
                ]
            );
        }
    }

    function update(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->id ,
            'role' => ['required'],
        ]);

        if ($validator->fails()) {    
            return response()->json($validator->messages());
        }

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        $user->role_id = $request->role;
        if($user->save()){
            return response()->json(
                [
                    'status' => 200,
                    'message' => "Data updated successfully"
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => "Data was not updated!"
                ]
            );
        }
    }

    function delete(Request $request){
        // DB::table('users')
        //     ->where('id', $request->id)
        //     ->update(['active' => 0]);
        $user = User::find($request->id);
        $user->active = 0;
        if($user->save()){
            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Deleted successfully',
                ]
            );
        }else{
            return response()->json(
                [
                    'status' => 500,
                    'message' => 'Unable to delete this recored!',
                ]
            );
        }
    }
}
