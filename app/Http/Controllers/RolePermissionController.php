<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\RolePermission;
use App\Models\Role;

class RolePermissionController extends Controller
{
    public function index(Request $request, $id){
        $data['role'] = Role::find($id);
        $data['role_permissions'] = RolePermission::join('permissions', 'permissions.id', 'role_permissions.permission_id')
        ->where('role_id', $id)
        ->select(
            'permissions.alias',
            'role_permissions.*'
        )
        ->get();
        return view('role_permissions.index' ,$data);

    }

    public function updatePermission(Request $request){
        // $rp = RolePermission::find($request->id);
        // if($request->col_name == 'view')
        //     $rp->view = $request->permission;

        RolePermission::where('id', $request->id)
        ->update(["$request->col_name" => $request->permission]);

        return RolePermission::find($request->id);

    }
}
