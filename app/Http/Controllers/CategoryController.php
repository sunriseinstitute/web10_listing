<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Models\Category;

class CategoryController extends Controller
{
    function index(Request $request){
        if(!checkPermission('category', 'view')){
            return redirect()->route('no_permission');
        }
        if ($request->ajax()) 
        {
            $data = Category::where('active', 1);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn_edit = '';
                    $btn_delete = '';
                    if(checkPermission('category', 'update')){
                        $btn_edit = "<button class='btn btn-warning' onclick='edit(". $row->id .", this)'>Edit</button>";
                    }
                    if(checkPermission('category', 'delete')){
                        $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    }
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('categories.index');
    }

}
