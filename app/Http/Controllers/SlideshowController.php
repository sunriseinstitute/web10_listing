<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Models\Slideshow;

class SlideshowController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = Slideshow::where('active', 1);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function($row){
                    $imgUrl = asset('assets/img').'/'.$row->photo;
                    return "<img src='$imgUrl' width='100px'>";
                })
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". json_encode($row) .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);
        }
        return view('slideshows.index');
    }

}
