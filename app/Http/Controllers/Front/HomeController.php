<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function home(){
        return view('fronts.home');
    }

    public function promotion(){
        $data['products'] = DB::table('product_promotions')
        ->join('products', 'products.id', 'product_promotions.product_id')
        ->join('categories', 'categories.id', 'products.category_id')
        ->where('products.active', 1)
        ->where('product_promotions.active', 1)
        ->select(
            'product_promotions.*',
            'products.name as product_name',
            'products.price as product_price',
            'products.photo as photo',
            'products.short_description as short_description',
            'categories.name as category_name'
        )
        ->orderBy('id', 'desc')
        ->paginate(12);
        return view('fronts.all_promotion', $data);
    }

    public function productCategory($id){
        $data['products'] = DB::table('products')
        ->join('categories', 'categories.id', 'products.category_id')
        ->where('products.active', 1)
        ->where('products.category_id', $id)
        ->select(
            'products.*',
            'categories.name as category_name'
        )
        ->orderBy('id', 'desc')
        ->paginate(12);
        $data['category'] = DB::table('categories')->find($id);
        return view('fronts.product_category', $data);
    }

    public function detail($id){
        $data['product'] = DB::table('products')
            ->join('categories', 'categories.id', 'products.category_id')
            ->where('products.id', $id)
            ->select(
                'products.*',
                'categories.name as cat_name',
                'categories.name as cat_id'
            )
            ->first();
        
        $p = DB::table('products')->find($id);

        $data['related_products'] = DB::table('products')
        ->where('products.id', '!=', $id)
        ->where('category_id', $p->category_id)
        ->orWhere('products.name', 'like', "%$p->name %")
        ->limit(12)->get();
        return view('fronts.product_detail', $data);
    }
}
