<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\ProductPromotion;
use DataTables;
use DB;

class ProductPromotionController extends Controller
{
    function index(Request $request){
        if ($request->ajax()) 
        {
            $data = ProductPromotion::join('products', 'products.id', 'product_promotions.product_id')
                ->join('categories', 'categories.id', 'products.category_id')
                ->where('products.active', 1)
                ->where('product_promotions.active', 1)
                ->select(
                    'product_promotions.*',
                    'products.name as product_name',
                    'products.price as product_price',
                    'products.photo as photo',
                    'categories.name as category_name'
                );
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function($row){
                    $src = asset('assets/img/'.$row->photo);
                    $img = "<img src='$src' width='50px'/>";
                    return $img;
                })
                ->addColumn('action', function($row){
                    $btn_edit = "<button class='btn btn-warning' onclick='edit(". $row->id .", this)'>Edit</button>";
                    $btn_delete = "<button class='btn btn-danger' onclick='showConfirm(". $row->id .", this)'>Delete</button>";
                    return $btn_edit. ' '. $btn_delete;
                })
                ->rawColumns(['action', 'photo'])
                ->make(true);
        }

        $data['categories'] = DB::table('categories')->where('active', 1)->get();
        return view('product_promotions.index', $data);
    }

}
