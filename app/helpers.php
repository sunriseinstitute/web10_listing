<?php 

    function getUsername(){
        return auth()->user()->name;
    }

    function checkPermission($permission_name, $action){

        $per = DB::table('role_permissions')
            ->join('permissions', 'permissions.id', 'role_permissions.permission_id')
            ->where('permissions.name', $permission_name)
            ->where('role_id', auth()->user()->role_id)
            ->select(
                "$action as permission"
            )
            ->first();

        
        if(@$per->permission == 1){
            return true;
        }else{
            return false;
        }

    }

    function is_mobile(){
        $is_mobile = false;
        if(((strpos(@$_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos(@$_SERVER['HTTP_USER_AGENT'], 'Safari/') == false)) || (@$_SERVER['HTTP_X_REQUESTED_WITH'] == "com.e_cpp_web")){
          $is_mobile = true;
        }
      
        return $is_mobile;
      }