@extends('layouts.master')
@section('custom-css')

@endsection
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Set Permission for {{$role->name}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('role.index')}}">Role</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered w-100 datatable" id="dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th> Name</th>
                            <th>View</th>
                            <th>Create</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($role_permissions as $k => $rp)
                        <tr>
                            <td>
                                {{$k+1}}
                            </td>
                            <td>{{$rp->alias}}</td>
                            <td>
                                <input type="checkbox" name="view[]" @if($rp->view == 1) checked @endif
                                onchange="savePermission(this, {{$rp->id}}, 'view')">
                            </td>
                            <td>
                                <input type="checkbox" name="create[]" @if($rp->create == 1) checked @endif
                                onchange="savePermission(this, {{$rp->id}}, 'create')">
                            </td>
                            <td>
                                <input type="checkbox" name="update[]" @if($rp->update == 1) checked @endif
                                onchange="savePermission(this, {{$rp->id}}, 'update')">
                            </td>
                            <td>
                                <input type="checkbox" name="delete[]" @if($rp->delete == 1) checked @endif
                                onchange="savePermission(this, {{$rp->id}}, 'delete')">
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>


<!-- /.content -->
@endsection

@section('custom-js')
<script>
function savePermission(e, id, type) {
    $.ajax({
        type: 'post',
        url: "{{route('role_permission.updatePermission')}}",
        data: {
            "_token": "{{ csrf_token() }}",
            id: id,
            col_name: type,
            permission: e.checked == true ? 1: 0
        },
      
        dataType: 'json',
        success: function(response) {
           console.log(response);
        }
    });
}
</script>

@endsection