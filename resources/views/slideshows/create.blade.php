<div class="modal fade" id="createNew" tabindex="-1" aria-labelledby="createNewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="formSubmit" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="table_name" value="slideshows">
            <div class="modal-header">
                <h5 class="modal-title" id="createNewLabel">Create New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="name">Link: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="link">
                            <span class="text-danger error_sms" id="error_link"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Description: <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                            <span class="text-danger error_sms" id="error_description"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Photo: <span class="text-danger">*</span></label>
                            <input type="file" name="photo" id="photo" class="form-control" required>
                            <span class="text-danger error_sms" id="error_file"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>