<div class="modal fade" id="editForm" tabindex="-1" aria-labelledby="editFormLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="editFormSubmit" method="post">
            @csrf
            <input type="hidden" name="id" id="eid">
            <input type="hidden" name="table_name" value="categories">
            <div class="modal-header">
                <h5 class="modal-title" id="editFormLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" id="ename" required>
                            <span class="text-danger error_sms" id="error_name"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>