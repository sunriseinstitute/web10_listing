<div class="modal fade" id="createNew" tabindex="-1" aria-labelledby="createNewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="formSubmit" action="{{route('user.save')}}" method="post">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="createNewLabel">Create New user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" required>
                            <span class="text-danger error_sms" id="error_name"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Username: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="username" required>
                            <span class="text-danger error_sms" id="error_username"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Email: <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email" required>
                            <span class="text-danger error_sms" id="error_email"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Password: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="password" required>
                            <span class="text-danger error_sms" id="error_password"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name">Role: <span class="text-danger">*</span></label>
                            <select name="role" id="role" class="form-control" required>
                                <option value="">Select an Opton</option>
                                @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                            <span class="text-danger error_sms" id="error_role"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>