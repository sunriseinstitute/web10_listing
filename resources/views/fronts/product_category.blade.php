@extends('layouts.fronts.master')
@section('content')

@if(count($products) > 0)
<section id="section_product" class="container mb-3 mt-3 card p-0 bg-white shadow-lg border-0">
    <div class="card-body ">
        <div class="row px-2">
            <div class="col-sm">
                <h4 class=" text-main">Product: {{$category->name}} </h4>
                <hr>
            </div>
        </div>
        <div class="row px-2">
            @foreach($products as $product)
            <div class="col-sm-3 col-6 mb-3">
                <div class="card shadow-sm">
                    <div class="img-boxes">
                        <img src="{{asset('assets/img/'.$product->photo)}}" alt="" class="w-100 rounded">
                    </div>
                    <div class="card-body">
                        <div class="short-text">
                            <p class="card-text">
                                {!! $product->short_description !!}
                            </p>
                        </div>

                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="text-main">${{number_format($product->price, 2)}}</h4>
                            <a href="{{route('web.detail', $product->id)}}" class="btn btn-sm btn-main">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row px-2">
            <div class="col">
                {{ $products->links() }}
            </div>
        </div>
    </div>
</section>
@else
<center>
    <br><br><br>
    <h4>There is no product to show.</h4>
</center>
@endif

@endsection

@section('custom-js')
    <script>
        var paginateUrl = window.location.href;    
            if (paginateUrl.indexOf('?') > -1){
                paginateUrl += '#section_product'
                window.location.href = paginateUrl;
            }
    </script>
@endsection