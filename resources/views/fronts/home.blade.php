@extends('layouts.fronts.master')
@section('content')
    <!-- slideshow section  -->
    @include('fronts.partials.slide')
    <!-- Promotion  -->
    @include('fronts.partials.promotion')
    <!-- All Product  -->
    @include('fronts.partials.all_product')

@endsection

@section('custom-js')
    <script>
        $('.carousel .carousel-item').each(function(){
            var minPerSlide = 4;
            var next = $(this).next();
            if (!next.length) {
            next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
            
            for (var i=0;i<minPerSlide;i++) {
                next=next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                
                next.children(':first-child').clone().appendTo($(this));
            }
        });

        var paginateUrl = window.location.href;    
            if (paginateUrl.indexOf('?') > -1){
                paginateUrl += '#section_product'
                window.location.href = paginateUrl;
            }
    </script>
@endsection