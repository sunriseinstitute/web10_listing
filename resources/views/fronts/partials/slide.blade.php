<?php 
    $slide_shows = DB::table('slideshows')->where('active', 1)->get();
?>
<section class="py-3">
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            @foreach($slide_shows as $i => $slide)
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$i}}" @if($i==0) class="active" @endif
                aria-current="true" aria-label="Slide 1"></button>
            @endforeach
        </div>
        <div class="carousel-inner">
            @foreach($slide_shows as $k => $slide)
            <div class="carousel-item @if($k == 0) active @endif">
                <img src="{{asset('assets/img/'.$slide->photo)}}" class="d-block w-100" alt="No Image">
            </div>
            @endforeach
            
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</section>