<?php 
    $promotions = DB::table('product_promotions')
        ->join('products', 'products.id', 'product_promotions.product_id')
        ->join('categories', 'categories.id', 'products.category_id')
        ->where('products.active', 1)
        ->where('product_promotions.active', 1)
        ->select(
            'product_promotions.*',
            'products.name as product_name',
            'products.price as product_price',
            'products.photo as photo',
            'products.short_description as short_description',
            'categories.name as category_name'
        )
        ->orderBy('id', 'desc')
        ->limit(8)
        ->get();

    
?>
@if(count($promotions) > 0)
<section class="container mb-3 card p-0 bg-white shadow-lg border-0">
    <div class="card-body">
        <div class="container">
            <div class="row px-2">
                <div class="col-6">
                    <h4 class="text-main">PROMOTIONS</h4>
                </div>
                <div class="col-6" style="positoin: relative; z-index: 99999">
                    <a href="{{route('web.promotion')}}" class="btn btn-sm btn-main float-end">MORE</a>
                </div>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide container" data-bs-ride="carousel">
            <div class="carousel-inner">
              @foreach($promotions as $promotion)
              @for($i=0; $i<10; $i++) 
                  <div class="carousel-item @if($i==0) active @endif">
                    <div class="col-md-3 p-2">
                        <div class="card shadow-sm">
                            <div class="" style="height : 200px; overflow: hidden">
                                <img src="{{asset('assets/img/'.$promotion->photo)}}" alt="" class="w-100 rounded">
                            </div>
                            <div class="card-body">
                                <div class="short-text">
                                    <p class="card-text">
                                        {!! $promotion->short_description !!}
                                    </p>
                                </div>

                                <div class="d-flex justify-content-between align-items-center " >
                                    <h4 class="text-main">${{number_format($promotion->product_price, 2)}}</h4>
                                    <a href="{{route('web.detail', $promotion->id)}}" class="btn btn-sm btn-main" style="positoin: relative; z-index: 99999">Detail</a>
                                </div>
                            </div>

                        </div>
                    </div>
                  </div>
              @endfor
              @endforeach
            </div>
        </div>
        <button class="carousel-control-prev" 
          type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon bg-main rounded-circle shadow-sm" 
             
             aria-hidden="true">
            </span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon bg-main rounded-circle shadow-sm" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</section>
@endif