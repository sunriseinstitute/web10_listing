@extends('layouts.fronts.master')
@section('content')

<section class="container card my-3 shadow-sm">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6 col-12">
                <div style="height: 350px; overflow: hidden">
                    <img src="{{asset('assets/img/'. $product->photo)}}" class="rounded" alt="" width="100%">
                </div>
            </div>
            <div class="col-sm-6 col-12">
                <h4>{{$product->name}}</h4>
                <small><i>Category: {{$product->cat_name}}</i></small>
                <h3 class="text-main">Price: ${{number_format($product->price, 2)}}</h3>
                <hr>
                <h4>Contact Us:</h4>
                <div class="row">
                    <div class="col ms-3">
                        <p>Phone: 085 35 6767</p>
                        <p>Email: rithysam.sr@gmail</p>
                    </div>
                </div>
                <div class="row">
                <div class="col ms-3">
                       <a href="https://t.me/Rithysam">
                       <img src="{{asset('assets/fronts/img/telegram.png')}}" alt="" class="rounded-circle" width="50px">
                       </a>
                       <a href="https://www.facebook.com/vdoo.freelancer">
                       <img src="{{asset('assets/fronts/img/messenger.png')}}" alt=""  width="50px">
                       </a>
                    </div>
                </div>

            </div>
        </div>
        <div class="row mt-5">
            <div class="col-sm">
                <h4 class="text-main">Descriptoin:</h4>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm">
                {!! $product->description !!}
            </div>
        </div>
    </div>
</section>

<section class="container card my-3 shadow-sm">
    <div class="card-body"> 
        <div class="row">
            <div class="col-sm">
                <h4 class="text-main">Related Product:</h4>
            </div>
        </div>
        <div class="row">
        @foreach($related_products as $related_product)
            <div class="col-sm-3 col-6 mb-3">
                <div class="card shadow-sm">
                    <div class="img-boxes">
                        <img src="{{asset('assets/img/'.$related_product->photo)}}" alt="" class="w-100 rounded">
                    </div>
                    <div class="card-body">
                        <div class="short-text">
                            <p class="card-text">
                                {!! $related_product->short_description !!}
                            </p>
                        </div>

                        <div class="d-flex justify-content-between align-items-center">
                            <h4 class="text-main">${{number_format($related_product->price, 2)}}</h4>
                            <a href="{{route('web.detail', $related_product->id)}}" class="btn btn-sm btn-main">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@endsection
