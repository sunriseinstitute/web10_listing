<div class="modal fade" id="createNew" tabindex="-1" aria-labelledby="createNewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="formSubmit" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="table_name" value="products">
            <div class="modal-header">
                <h5 class="modal-title" id="createNewLabel">Create New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-sm">
                        <label for="name">Category: <span class="text-danger">*</span></label>
                        <select name="category_id" id="category_id" class="form-control" required>
                            <option value="">------</option>
                            @foreach($categories as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger error_sms" id="error_category_id"></span>
                    </div>
                    <div class="form-group col-sm">
                        <label for="name">Code: <span class="text-danger"></span></label>
                        <input type="text" class="form-control" name="code">
                        <span class="text-danger error_sms" id="error_code"></span>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="form-group col-sm">
                        <label for="name">Name: <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="name" required>
                        <span class="text-danger error_sms" id="error_name"></span>
                    </div>
                    <div class="form-group col-sm">
                        <label for="name">Price: <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="price" required>
                        <span class="text-danger error_sms" id="error_price"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label for="name">Short Description: <span class="text-danger">*</span></label>
                            <textarea name="short_description" id="short_description" class="form-control summernote"
                                rows="3" required></textarea>
                            <span class="text-danger error_sms" id="error_short_description"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Full Description: <span class="text-danger">*</span></label>
                            <textarea name="description" id="description" class="form-control summernote" rows="3"
                                required></textarea>
                            <span class="text-danger error_sms" id="error_description"></span>
                        </div>
                        <div class="form-group">
                            <label for="name">Photo: <span class="text-danger">*</span></label>
                            <input type="file" name="photo" id="photo" class="form-control" required>
                            <span class="text-danger error_sms" id="error_photo"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>