<!-- Modal -->
<div class="modal fade" id="comfirmDeleteModal" tabindex="-1" aria-labelledby="comfirmDeleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="comfirmDeleteModalLabel">Delete Confimration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure to delete this record?
      </div>
      <input type="hidden" name="deletedId" id="deletedId">
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="button" onclick="remove()" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>