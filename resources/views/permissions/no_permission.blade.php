@extends('layouts.master')
@section('custom-css')

@endsection
@section('content')

<!-- Main content -->
<section class="content mt-5">
    <div class="container-fluid">
        <h1 class="text-center text-danger">
            You don't have permission to access this feature!
        </h1>
    </div><!-- /.container-fluid -->
</section>


<!-- /.content -->
@endsection
