<div class="modal fade" id="editForm" tabindex="-1" aria-labelledby="editFormLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="editFormSubmit" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="table_name" value="product_promotions">
            <input type="hidden" name="id" id="eid">
            <div class="modal-header">
                <h5 class="modal-title" id="editFormLabel">Edit Promotion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-sm">
                        <label for="name">Product: <span class="text-danger">*</span></label>
                        <select name="product_id" id="eproduct_id" class="form-control" required>
                            <option value="">------</option>
                            <?php 
                                $products = DB::table('products')->where('active', 1)->get();
                            ?>
                            @foreach($products as $pro)
                            <option value="{{$pro->id}}">{{$pro->name}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger error_sms" id="error_category_id"></span>
                    </div>
                    <div class="form-group col-sm">
                        <label for="">Discount (%)</label>
                        <input type="number" step="0.01" min="0" name="discount" id="ediscount" value="0" class="form-control" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm">
                        <label for="estart_date">Start Date: <span class="text-danger">*</span></label>
                        <input type="date" class="form-control" name="start_date" id="estart_date" required>
                        <span class="text-danger error_sms" id="error_start_date"></span>
                    </div>
                    <div class="form-group col-sm">
                        <label for="eend_date">End Date: <span class="text-danger">*</span></label>
                        <input type="date" class="form-control" name="end_date" id="eend_date" required>
                        <span class="text-danger error_sms" id="error_end_date"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>