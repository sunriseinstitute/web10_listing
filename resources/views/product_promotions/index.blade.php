@extends('layouts.master')
@section('custom-css')

@endsection
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Product Promotion List</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Product Promotion</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-sm">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createNew">
                    Create
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered w-100 datatable" id="dataTable">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>No</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

@include('product_promotions.create')
@include('product_promotions.edit')
@include('product_promotions.detele')

<!-- /.content -->
@endsection

@section('custom-js')
<script>
$(document).ready(function() {
    //load data 
    var table = $('#dataTable').DataTable({
        aaSorting: [
            [0, "desc"]
        ],
        pageLength: 10,
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: "{{route('promotion.index')}}",
            data: function(d) {
                d.usernaem = ''
            }
        },
        columns: [{
                data: 'id',
                name: 'id',
                searchable: false,
                orderable: true,
                visible: false,
            },
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false
            },
            {
                data: 'photo',
                name: 'photo',
                orderable: false,
                searchable: false
            },
            {
                data: 'product_name',
                name: 'products.name'
            },
            {
                data: 'product_price',
                name: 'products.price'
            },
            {
                data: 'discount',
                name: 'product_promotions.discount'
            },
            {
                data: 'start_date',
                name: 'product_promotions.start_date'
            },
            {
                data: 'end_date',
                name: 'product_promotions.end_date'
            },
            {
                data: 'category_name',
                name: 'categories.name'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            }
        ],
    });

    // $("#btn_filter").click(function(){
    //     table.draw(true);
    // })

});
</script>

<script>
$(document).ready(function() {
    $('#formSubmit').submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'post',
            url: "{{route('bulk.save')}}",
            // data: $('#formSubmit').serialize(),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response) {
                console.log(response);
                if (response.status == 200) {
                    $('#dataTable').DataTable().ajax.reload();
                    $('#formSubmit')[0].reset();
                    $('.error_sms').text('');
                    $('#createNew').modal('hide');
                    alertMessage('success', response.message);
                }else if(response.status == 500) {
                    $('#createNew').modal('hide');
                    alertMessage('success', response.message);
                }else{
                    for (var key in response) {
                        $(' #error_'+key , '#createNew').text(response[key]);
                    }
                }
            }
        });
    });

    $('#editFormSubmit').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: "{{route('bulk.update')}}",
            data: $('#editFormSubmit').serialize(),
            dataType: 'json',
            success: function(response) {
                console.log(response)
                if (response.status == 200) {
                    $('#dataTable').DataTable().ajax.reload();
                    $('#editFormSubmit')[0].reset();
                    $('#editForm').modal('hide');
                    alertMessage('success', response.message)
                } else if (response.status == 500) {
                    alertMessage('success', response.message)
                }else{
                    for (var key in response) {
                        $(' #error_'+key , '#editForm').text(response[key]);
                    }
                }
            }
        });
    });

    // condfirm delete
    // $("#confirmDelete").click(function() {
    //     var id = $("#delete_id").val();
    //     remove(id);
    // });


});
function edit(id) {
    $.ajax({
        type: 'post',
        url: "{{route('bulk.getone')}}",
        data: {
            id: id,
            table_name: 'product_promotions',
            "_token": "{{ csrf_token() }}",
        },
        dataType: 'json',
        success: function(response) {
            console.log(response)
            if (response.status == 200) {
                var data = response.data;
                $('#editForm').modal('show');
                $('#eid').val(data.id)
                $('#eproduct_id').val(data.product_id)
                $('#ediscount').val(data.discount)
                $('#estart_date').val(data.start_date)
                $('#eend_date').val(data.end_date)
            }else{
                alertMessage('success', response.message)
            }
        }
    });

   
}


function showConfirm(id) {
    $("#deletedId").val(id);
    $('#comfirmDeleteModal').modal('show');

}

function remove() {
    var id = $("#deletedId").val();
    $.ajax({
        type: 'post',
        url: "{{route('bulk.delete')}}",
        data: {
            id: id,
            table_name: 'product_promotions',
            _token: "{{csrf_token()}}"
        },
        dataType: 'json',
        success: function(response) {
            console.log(response)
            if (response.status == 200) {
                
                $('#dataTable').DataTable().ajax.reload();
                $('#comfirmDeleteModal').modal('hide');
                alertMessage('success', response.message)
                // $('#editFormSubmit')[0].reset();
                // $('#closeConfirmDeleteModal').trigger('click');
                // $('#successMessage').show();
            } else {
                alertMessage('error', response.message)
            }
        }
    });
}
</script>
@endsection