<nav class="navbar navbar-expand-lg navbar-light bg-light bg-main">
  <div class="container-fluid">
    <a class="navbar-brand bg-white p-1 rounded" href="{{route('web.home')}}">
      <img src="{{ asset('assets/fronts/img/logo.png') }}" alt="" width="30" height="24" class="d-inline-block align-text-top">
      <span class="text-main"> SG-STORE </span>
    </a>

    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item @if(request()->category_id == '' && !request()->id) border-bottom @endif">
          <a class="nav-link active" aria-current="page" href="{{route('web.home')}}">Home</a>
        </li>
        <?php 
          $menu_categories = DB::table('categories')->where('active', 1)->get();
        ?>
        @foreach($menu_categories as $cat)
        <li class="nav-item @if(request()->category_id == $cat->id) border-bottom @endif">
          <a class="nav-link" href="{{route('web.category', $cat->id)}}">{{$cat->name}}</a>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</nav>